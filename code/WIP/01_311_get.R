#!/usr/bin/env Rscript
cat("\014")

# This script downloads 311 complaints data from NYC Open Data, archives it
# locally, and creates the "universe" we need for our analyses & graphics.
#
# See `Data-Decisions.Rmd` for the reasoning behind the filter criteria used to
# create our universe of closed 311 social distancing complaints.

library(dplyr)
library(glue)
library(readr)

# Earliest date for which we want data. Must be in Socrata format.
START_DATE = "2020-03-01T00:00:00.000"
# No slash at end
WRITE_DIR = "data/raw/311-Service-Requests-from-2010-to-Present"
# APP_TOKEN = Sys.getenv("NYCOD_SNAPSHOTS_TOKEN")
#
# library(RSocrata)
# read.socrata(glue("https://data.cityofnewyork.us/resource/erm2-nwe9.csv",
#                   "?$select=:*,*",
#                   "&$where=created_date>='{START_DATE}'"),
#              app_token = APP_TOKEN)
#
# c311 = read_csv(glue("https://data.cityofnewyork.us/resource/erm2-nwe9.csv",
#                      "?$select=:*,*",
#                      "&$where=created_date>='{START_DATE}'",
#                      "&$limit=1000000",
#                      "&$$app_token={APP_TOKEN}"))
#
# parse_dt = function(x) strftime(x, "%Y-%m-%d")
#
# start_ds = parse_dt(min(c311_uni$`Created Date`))
# end_ds   = parse_dt(max(c311_uni$`Created Date`))
# write_csv(c311_uni,
#           glue("{WRITE_DIR}/311_{start_ds}_{end_ds}_socdis_closed.csv"))
