# 311 Social Distancing Enforcement

This repo contains data and code used to analyze patterns of social distancing
enforcement by the NYPD.

## Contents

- See [Data Sources](data-sources.md) for a mostly comprehensive overview of the
  raw data sources
  
- `code/` contains the code used to clean the raw data and build the figures
  and widgets

  - Scripts with names beginning with a 2-digit number (e.g., `02_clean.R`)
    build everything. They were run in numeric order (`01_<name>.R`, then
    `02_<name>.R`, and so on)

  - Scripts with names not beginning with a number are one-off works in progress
    or diagnostics
    
  - `02_clean.R` is probably of most interest. It cleans all of the 311 data,
    NYPD-released summons data, and Legal Aid internal arrests data, joins them
    with each other and other datasets, builds shapefiles and rankings tables,
    and more.

- `data/raw` contains the raw data source archives

- `data/` contains the cleaned datasets built from `data/raw`

  - `data/shapefiles` contains the shapefile built from `data/raw` used to
    build all maps in `figs/`. Shapefile built using `311_precincts.csv`,
    detailed below
  
  - `311_complaints.csv` contains all 311 complaints meeting the criteria
    specified in the *Universe* section below
    
  - `311_precincts.csv` contains summary variables for all police precincts.
    Includes 311 complaints total and rates, NYPD-released summonses data
    (goes beyond summonses reported in 311 data), and counts of arrests tracked
    internally by Legal Aid.
    
  - `311_precincts_DICT.csv` is the data dictionary for `311_complaints.csv`
  
  - `311_rank_action.csv` is a table containing rankings of each precinct by
    311 social distnacing calls for which police reportedly "took action" (see
    [here](https://gitlab.com/everetr/covid-311/-/issues/4) for definition)
    
  - `sqf.csv` contains stop-question-frisk data extracted from NYCLU reports.
    Also includes precinct population estimates (see [here](https://gitlab.com/everetr/covid-311/-/issues/2))
    for more info

- `figs` contains static images built from `data/`. E.g., maps

  - `CHLORO_311.png` is a map of 311 social distancing complaints (see
    *Universe* section below) per 10,000 residents, for each police precinct
    
  - `CHLORO_summons-arrest.png` is a map of summonses issued and arrests made
    for social distancing violations

  - `figs/widgets` contains interactive HTML widgets built from `data/`. E.g.,
    interactive maps
    
    - `CHLORO_311.html` is the interactive version of `CHLORO_311.png`
    
    - `CHLORO_summons-arrest.html` is the interactive version of
      `CHLORO_summons-arrest.png`
      
    - Other widgets are exploratory and unreleased
    
## Universe

We filter 311 complaints to only include those fitting these criteria:

- Regarding "Social Distancing"

- "Closed" status

- Longitude and latitude are not missing
