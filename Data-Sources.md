# Data Sources

## Files

### `data/raw/COVID-19 Summons Breakdown 03-16-2020 to 05-05-20.xlsx`

NYPD-released social distancing summonses data. Provided by The Gothamist.

### `data/raw/unique_arrests.csv`

Variable `int_arr` contains "internal arrests," or social distancing arrests
tracked internally by Legal Aid, based on cases.

### `data/raw/Police-Precincts/*`

[NYC Open Data: Police Precincts](https://data.cityofnewyork.us/Public-Safety/Police-Precincts/78dh-3ptz). Downloaded 2020-04-12.

Used to create all files in `data/shapefiles/`.

### `data/raw/311-Service-Requests-from-2010-to-Present/*`

[NYC Open Data: 311 Service Requests from 2010 to Present](https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9)

Used to create files in `data/`:

- `311_complaints.csv`
- `311_precincts.csv`
- `311_rank_action.csv`
- All files in `shapefiles/`

### `data/raw/20190314_nyclu_stopfrisk_singles.pdf`

NYCLU (2019). [*Stop-and-Frisk in the de Blasio Era*](https://www.nyclu.org/en/publications/stop-and-frisk-de-blasio-era-2019). Direct download link [here](https://www.nyclu.org/sites/default/files/field_documents/20190314_nyclu_stopfrisk_singles.pdf). Downloaded 2020-05-05.

### `data/raw/20190314_nyclu_stopfrisk_appendices_onlineonly.pdf`

NYCLU (2019). [*Appendix: Stop-and-Frisk in the de Blasio Era*](https://www.nyclu.org/en/publications/stop-and-frisk-de-blasio-era-2019).
Direct download link [here](https://www.nyclu.org/sites/default/files/field_documents/20190314_nyclu_stopfrisk_appendices_onlineonly.pdf). Downloaded 2020-04-09.

Used to create files in `data/`:

- `sqf.csv`
- `311_rank_action.csv`

### `data/raw/stopandfrisk_briefer_2002-2013_final.pdf`

NYCLU (2014). [*Stop-and-Frisk During the Bloomberg Administration 2002-2013*](https://www.nyclu.org/en/publications/stop-and-frisk-during-bloomberg-administration-2002-2013-2014).
Direct download link [here](https://www.nyclu.org/sites/default/files/publications/stopandfrisk_briefer_2002-2013_final.pdf).

Not yet used.

## Variables

### Precinct neighborhood labels

Extracted from:

> NYCLU (2019). [*Stop-and-Frisk in the de Blasio Era*](https://www.nyclu.org/en/publications/stop-and-frisk-de-blasio-era-2019). Direct download link [here](https://www.nyclu.org/sites/default/files/field_documents/20190314_nyclu_stopfrisk_singles.pdf). Downloaded 2020-05-05.

### Precinct 311 complaints

Calculated using:

>[NYC Open Data: 311 Service Requests from 2010 to Present](https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9)

### Precinct population

Estimated using:

> NYCLU (2019). [*Appendix: Stop-and-Frisk in the de Blasio Era*](https://www.nyclu.org/en/publications/stop-and-frisk-de-blasio-era-2019).
Direct download link [here](https://www.nyclu.org/sites/default/files/field_documents/20190314_nyclu_stopfrisk_appendices_onlineonly.pdf). Downloaded 2020-04-09.

This file provides 1) Total stops, and 2) Stops as a % of population (US Census decennial, 2010). I calculate approximate population with these variables.
